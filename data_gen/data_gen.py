
path = 'repo_kruglov/'
filename_date = 'data_gen/examples_date.txt'
filename_period = 'data_gen/examples_period.txt'
filename_greet = 'data_gen/examples_greet.txt'
filename_peoples = 'data_gen/examples_peoples.txt'
filename_nlu = 'data/autogen_period.yml'

 
def read_file(filename):
 #with_open
    f = open(path+filename,'r')

    lines = []
    
    for line in f:
        lines.append(line.strip())
    
    f.close()
    return lines

def combination_date1_date2(periods, dates):
    list_str = []
    for period in periods :
        for date in dates :
            period_str = period
            date_str1 = '[' + date + ']{"entity": "date", "role": "arrival"}'
            date_str2 = '[' + date + ']{"entity": "date", "role": "departure"}'
            period_str = period_str.replace('date1', date_str1) 
            period_str = period_str.replace('date2', date_str2) 
            list_str.append(period_str)
    return list_str

def combination_greet_date1_date2(list_date1_date2, greets):
    ind_greet = 0
    len_greet = len(greets)
    list_str = []
    for date1_date2 in list_date1_date2 :
        greet = greets[ind_greet]
        list_str.append(f'{greet}. {date1_date2}')

        if ind_greet < len_greet - 1:
            ind_greet += 1
        else:
            ind_greet = 0

    return list_str

def combination_peoples_count(list_peoples):

    list_name = ['', 'человек', 'гостей']

    ind_name = 0
    len_name = len(list_name)
    list_str = []
    for peoples in list_peoples :
        name = list_name[ind_name]
        list_str.append(f'[{peoples}](peoples_count) {name}')

        if ind_name < len_name - 1:
            ind_name += 1
        else:
            ind_name = 0

    return list_str

def combination_greet_date1_date2_peoples_count(list_greet_date1_date2, list_peoples_count):
    ind_peoples_count = 0
    len_peoples_count = len(list_peoples_count)
    list_str = []
    for greet_date1_date2 in list_greet_date1_date2 :
        peoples_count = list_peoples_count[ind_peoples_count]
        list_str.append(f'{greet_date1_date2}. {peoples_count}')

        if ind_peoples_count < len_peoples_count - 1:
            ind_peoples_count += 1
        else:
            ind_peoples_count = 0

    return list_str

def save_file_nlu(filename, intents):
    text_file = 'version: "3.1"\nnlu:'
    
    for intent in intents:
        text_file = text_file + text_intent(intent[0], intent[1])

    # text_file = text_file + text_intent('greet', list_greet)
    # text_file = text_file + text_intent('date1_date2', list_date1_date2)
    # text_file = text_file + text_intent('greet+date1_date2', list_greet_date1_date2)
    
    f = open(path+filename,'w') 
    f.write(text_file) 
    f.close()

def text_intent(name_intent, examples):
    text_intent = '\n\n- intent: ' + name_intent + '\n  examples: |'
    for example in examples:
        text_intent = text_intent + '\n    - ' + example
    return text_intent

dates = read_file(filename_date)
periods = read_file(filename_period)
greets = read_file(filename_greet)
peoples = read_file(filename_peoples)

list_date1_date2 = combination_date1_date2(periods, dates)
list_greet_date1_date2 = combination_greet_date1_date2(list_date1_date2, greets)
list_peoples_count = combination_peoples_count(peoples)
list_greet_date1_date2_peoples_count = combination_greet_date1_date2_peoples_count(list_greet_date1_date2, list_peoples_count)

intents = []
intents.append(['greet', greets])
intents.append(['date1_date2', list_date1_date2])
intents.append(['greet+date1_date2', list_greet_date1_date2])
intents.append(['peoples_count', list_peoples_count])
intents.append(['greet+date1_date2+peoples_count', list_greet_date1_date2_peoples_count])


save_file_nlu(filename_nlu, intents)


