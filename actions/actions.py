# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from asyncio import events
from pkgutil import get_data
from typing             import Any, Text, Dict, List
#
from rasa_sdk           import Action, Tracker, ValidationAction
from rasa_sdk.events    import SlotSet
from rasa_sdk.executor  import CollectingDispatcher
from rasa_sdk.types     import DomainDict
from pathlib            import Path

from datetime           import datetime as dt
import json

class ValidatePredefinedSlots(ValidationAction):
    def validate_arrival(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
        ) -> Dict[Text, Any]:

        value = self.validate_date(slot_value)
        return {"arrival": value}

    def validate_departure(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
        ) -> Dict[Text, Any]:

        value = self.validate_date(slot_value)
        return {"departure": value}

    def validate_date(self, slot_value):
        months = {
            "01": "январь,января,янв,ян",
            "02": "февраля,февраль,фев,фе,февр",
            "03": "марта,март,мар",
            "04": "апреля,апрель,апр,ап",
            "05": "мая,май",
            "06": "июня,июнь,июн",
            "07": "июля,июль,июл",
            "08": "августа,август,авг,ав",
            "09": "сентября,сентябрь,сен,се,сенябрь,сенября",
            "10": "октября,октябрь,окт,ок",
            "11": "ноября,ноябрь,нояб, ноя",
            "12": "декабря,декабрь,дек,де,декаб"
        }

        slot_value = " ".join(slot_value.split())  # удаление повторяющихся пробелов

        slot_value = slot_value.replace('-', '.')
        slot_value = slot_value.replace('/', '.')
        slot_value = slot_value.replace(' ', '.')
        slot_value = slot_value.lower()  

        arr = slot_value.split('.')

        current_date = dt.today()
        year = current_date.year

        if len(arr) < 2 :
            return None
        elif len(arr) == 2 :
            arr.append(str(year))

        arr[0] = arr[0].zfill(2)
        
        if arr[1].isdigit() :
            arr[1] = arr[1].zfill(2) 
        elif arr[1].isalpha() :
            for key in months:
                if months[key].find(arr[1]) != -1:
                    arr[1] = key
                    break
            else:
                return None
        else:
            return None

        if arr[2].isdigit() :
            if len(arr[2]) == 2:
                arr[2] = '20' + arr[2]
            if len(arr[2]) != 4:
                return None
        else:
            return None

        try:
            date = dt(int(arr[2]), int(arr[1]), int(arr[0]))
            if date < current_date:
                arr[2] = str(year + 1)
                date = dt(int(arr[2]), int(arr[1]), int(arr[0]))
        except ValueError:
            return None

        result = '.'.join(arr) 


        return result

class ActionClearSlots(Action):
 
    def name(self) -> Text:              # регистрируем имя действия
        return "action_clear_slots"
 
    def run(self, dispatcher:CollectingDispatcher, tracker:Tracker, domain:Dict[Text,Any]) -> List[Dict[Text, Any]]:
 
        return [SlotSet("arrival", None), SlotSet("departure", None), SlotSet("peoples_count", None)]

class ActionClearPeriod(Action):
 
    def name(self) -> Text:              # регистрируем имя действия
        return "action_clear_period"
 
    def run(self, dispatcher:CollectingDispatcher, tracker:Tracker, domain:Dict[Text,Any]) -> List[Dict[Text, Any]]:
 
        return [SlotSet("arrival", None), SlotSet("departure", None)]

class ActionShowTime(Action):
 
    def name(self) -> Text:              # регистрируем имя действия
        return "action_show_time"
 
    def run(self, dispatcher:CollectingDispatcher, tracker:Tracker, domain:Dict[Text,Any]) -> List[Dict[Text, Any]]:
 
        # при вызове действия возвращаться ответ с текущим временем: 
        dispatcher.utter_message(text=f'Сейчас {dt.now().strftime("%H:%M")}')

        return []

class ActionSaveTracker(Action):
 
    def name(self) -> Text:              # регистрируем имя действия
        return "action_save_tracker"
 
    def run(self, dispatcher:CollectingDispatcher, tracker:Tracker, domain:Dict[Text,Any]) -> List[Dict[Text, Any]]:
      
        json_string = self.get_data(tracker)
    
        self.data_print(dispatcher, json_string)

        return []

    def get_data(self, tracker):

        count_list = len(tracker.events)
        events = []

        for i in range(max(0, count_list-8), max(0, count_list-3)):
            events.append(tracker.events[i])

        data = {}
        data['sender_id']           = tracker.sender_id
        data['slots']               = tracker.slots
        data['events']              = events 

        json_string = json.dumps(data, indent=4, skipkeys=True).encode('utf-8').decode('unicode-escape')
        return json_string

    def data_print(self, dispatcher, json_string):

        # file_name = Path.cwd().parent / 'trackers' / f'tracker_{dt.now().strftime("%H:%M:%S")}.dat'
        file_name = self.get_file_name()
        try:  
            f = open(file_name,'w') 
            f.write(json_string) 
            dispatcher.utter_message(text=f'Данные выгружены в файл {file_name}')
        except:
            dispatcher.utter_message(text=f'Ошибка записи файла')
        finally:
            f.close()

    def get_file_name(self):
        file_name = Path.cwd().parent / 'trackers' / f'tracker_{dt.now().strftime("%Y-%m-%d %H:%M:%S")}.dat'
        return file_name    

class ActionSaveTrackerAll(ActionSaveTracker):
 
    def name(self) -> Text:              # регистрируем имя действия
        return "action_save_tracker_all"
    
    def get_data(self, tracker):
        data = {}
        data['sender_id'] = tracker.sender_id
        data['slots'] = tracker.slots
        data['events'] = tracker.events 

        json_string = json.dumps(data, indent=4, skipkeys=True).encode('utf-8').decode('unicode-escape')
        return json_string

    def get_file_name(self):
        file_name = Path.cwd().parent / 'trackers' / f'tracker_{dt.now().strftime("%Y-%m-%d %H:%M:%S")}_all.dat'
        return file_name 

